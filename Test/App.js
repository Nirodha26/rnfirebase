/*
Author: Andrew Seeley
Date: 21/11/17
Copyright © 2017 Ernst & Young. All rights reserved.

The root app component. The app launches and starts at this component.
*/

import React, { Component } from 'react';
import {
  NavigationActions,
  StackNavigator
} from 'react-navigation-awseeley--enhanced';
import { Platform, TouchableHighlight, Image } from 'react-native';
import { Provider } from 'react-redux';
// Imports for Push Notifications (i.e. FCM)
import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType
} from 'react-native-fcm';

// Import Redux Store requirements
import { createStore } from 'redux';
import reducer from './src/reducers';
import reduxStore from './src/ReduxStore/ReduxStore';
import { IOS, ANDROID } from './src/Constants/Constants';

import firebase from 'react-native-firebase';
import { FIREBASE_ANALYTICS_EVENTS } from './src/Constants/Constants';
import { InactivityTracker } from './src/Components/Molecules';
import { logger } from 'react-native-logger';

// Import screens
import {
  Splash,
  Help,
  MeetingChecklist,
  LoginSignUp,
  SignUpOptions,
  SignUpOptionsNewExisting,
  SignUpOptionsAccountType,
  TermsAndConditions,
  TermsAndConditionsFullPM,
  TermsAndConditionsPM,
  MyProfile,
  ManageParticipants,
  Settings,
  UploadPlan,
  Documents,
  AboutMe,
  WhereILive,
  FavouriteThings,
  HealthWellbeing,
  AboutLeapIn,
  TabRoot,
  RequestMemberAccessHome,
  RequestMemberAccessMessagePending,
  RequestMemberAccessMessageSuccess,
  WebViewer,
  PdfViewer,
  ChangePassword,
  Plans,
  BudgetsPayments,
  Crew,
  CrewCreateMember,
  Supports,
  AddProvider,
  ForgotPassword,
  Notifications,
  ApprovalsSummary,
  ApprovalsDetailsInvoice,
  ApprovalsDetailsServAgreement,
  PlanManagementTC,
  ChooseParticipantOrCrew,
  SpendingSupportItemDetail,
  PlanManagement,
  ProfilePictureForm,
  DocumentEditForm,
  DocumentAddForm,
  PlanSummaryPdfViewer
} from './src/Screens';

import * as NotificationActions from './src/Actions/NotificationActions';

// initalise redux store and return a reference to the store
const configureStore = () => createStore(reducer);
reduxStore.init(configureStore);
export const store = reduxStore.getStore();

export const LIApp = StackNavigator(
  {
    Splash: { screen: Splash },
    Help: { screen: Help },
    MeetingChecklist: { screen: MeetingChecklist },
    LoginSignUp: { screen: LoginSignUp },
    SignUpOptions: { screen: SignUpOptions },
    SignUpOptionsNewExisting: { screen: SignUpOptionsNewExisting },
    SignUpOptionsAccountType: { screen: SignUpOptionsAccountType },
    TermsAndConditions: { screen: TermsAndConditions },
    TermsAndConditionsFullPM: { screen: TermsAndConditionsFullPM },
    TermsAndConditionsPM: { screen: TermsAndConditionsPM },
    MyProfile: { screen: MyProfile },
    ManageParticipants: { screen: ManageParticipants },
    Settings: { screen: Settings },
    UploadPlan: { screen: UploadPlan },
    Documents: { screen: Documents },
    Supports: { screen: Supports },
    ChangePassword: { screen: ChangePassword },
    Plans: { screen: Plans },
    BudgetsPayments: { screen: BudgetsPayments },
    AboutMe: { screen: AboutMe },
    WhereILive: { screen: WhereILive },
    FavouriteThings: { screen: FavouriteThings },
    ForgotPassword: { screen: ForgotPassword },
    HealthWellbeing: { screen: HealthWellbeing },
    Crew: { screen: Crew },
    CrewCreateMember: { screen: CrewCreateMember },
    AboutLeapIn: { screen: AboutLeapIn },
    PlanManagementTC: { screen: PlanManagementTC },
    TabRoot: { screen: TabRoot },
    RequestMemberAccessHome: { screen: RequestMemberAccessHome },
    RequestMemberAccessMessagePending: {
      screen: RequestMemberAccessMessagePending
    },
    RequestMemberAccessMessageSuccess: {
      screen: RequestMemberAccessMessageSuccess
    },
    WebViewer: { screen: WebViewer },
    Notifications: { screen: Notifications },
    PdfViewer: { screen: PdfViewer },
    ApprovalsSummary: { screen: ApprovalsSummary },
    ApprovalsDetailsInvoice: { screen: ApprovalsDetailsInvoice },
    ApprovalsDetailsServAgreement: { screen: ApprovalsDetailsServAgreement },
    ChooseParticipantOrCrew: { screen: ChooseParticipantOrCrew },
    SpendingSupportItemDetail: { screen: SpendingSupportItemDetail },
    PlanManagement: { screen: PlanManagement },
    ProfilePictureForm: { screen: ProfilePictureForm },
    DocumentEditForm: { screen: DocumentEditForm },
    DocumentAddForm: { screen: DocumentAddForm },
    PlanSummaryPdfViewer: { screen: PlanSummaryPdfViewer }
  },

  {
    headerMode: 'none',
    cardStyle: { backgroundColor: 'transparent', opacity: 1 }
  }
);

// This adds an override to the router navigation.
// So long as you pass it a parameter called 'ReplaceCurrentScreen'
// It will replace the current screen with the target one on the stack.
// This way you can prevent the user from pressing
// the back button to go to the previous

const prevGetStateForActionHomeStack = LIApp.router.getStateForAction;
LIApp.router.getStateForAction = (action, state) => {
  if (action !== undefined) {
    if (action.params !== undefined) {
      if (action.params.navigateType === 'ReplaceCurrentScreen') {
        const routes = state.routes.slice(0, state.routes.length - 1);
        routes.push(action);
        return {
          ...state,
          routes,
          index: routes.length - 1
        };
      }
    }
  }
  return prevGetStateForActionHomeStack(action, state);
};

// Push Notification Handling
// this shall be called regardless of app state: running, background
// or not running. Won't be called when app is killed by user in iOS
FCM.on(FCMEvent.Notification, async notif => {
  logger.log('FCM::notification rec');
  // there are two parts of notif. notif.notification contains the
  // notification payload, notif.data contains data payload
  if (notif.local_notification) {
    // this is a local notification
    logger.log('FCM::local notification');
  }
  if (notif.opened_from_tray) {
    // iOS: app is open/resumed because user clicked banner
    // Android: app is open/resumed because user clicked banner or tapped app icon
    logger.log('FCM::user clicked banner or tapped app icon');
  }
  // await someAsyncCall();

  if (Platform.OS === IOS) {
    // optional
    // iOS requires developers to call completionHandler to end notification
    // process. If you do not call it your background remote notifications
    // could be throttled, to read more about it see
    // https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623013-application.
    // This library handles it for you automatically with default behavior
    // (for remote notification, finish with NoData; for WillPresent,
    // finish depend on "show_in_foreground"). However if you want to return
    // different result, follow the following code to override
    // notif._notificationType is available for iOS platfrom
    switch (notif._notificationType) {
      case NotificationType.Remote:
        // other types available: RemoteNotificationResult.NewData,
        // RemoteNotificationResult.ResultFailed
        logger.log(
          JSON.stringify(notif) + 'Remote Notification Payload only for iOS'
        );

        notification = JSON.parse(notif.custom_notification);

        // Open the remote notifications as local only for iOS
        FCM.presentLocalNotification({
          id: notification.id,
          title: notification.title,
          body: notification.body,
          color: notification.color,
          priority: notification.priority,
          show_in_foreground: true,
          click_action: 'com.myidentifi.fcm.tex',
          type: notification.type,
          link: notification.link,
          opened_from_tray: 0
        });

        notif.finish(RemoteNotificationResult.NewData);
        break;
      case NotificationType.NotificationResponse:
        notif.finish();
        break;
      case NotificationType.WillPresent:
        // other types available: WillPresentNotificationResult.None
        notif.finish(WillPresentNotificationResult.All);
        break;
      default:
        break;
    }
  }
});

FCM.on(FCMEvent.RefreshToken, token => {
  // fcm token may not be available on first load, catch it here
});

// React Native App component
class App extends Component {
  componentWillMount() {
    firebase.analytics().setAnalyticsCollectionEnabled(true);
    firebase
      .analytics()
      .logEvent(FIREBASE_ANALYTICS_EVENTS.APP_INITIALIZED, {});

    console.ignoredYellowBox = ['Remote debugger'];

    // iOS: show permission prompt for the first call. later just check permission in user settings
    // Android: check permission in user settings
    FCM.requestPermissions()
      .then(() => logger.log('FCM::notification permission granted'))
      .catch(() => logger.log('FCM::notification permission rejected'));

    FCM.getFCMToken().then(token => {
      // store fcm token in your server
    });

    if (Platform.OS === IOS) {
      FCM.getAPNSToken().then(token => {
        logger.log('FCM::APNS TOKEN (getFCMToken)', token);
      });
    }

    this.notificationListener = FCM.on(FCMEvent.Notification, async notif => {
      // optional, do some component related stuff
      logger.log('Started the notificationListener');
    });

    // initial notification contains the notification that launchs the app.
    // If user launchs app by clicking banner, the banner notification info
    // will be here rather than through FCM.on event
    // sometimes Android kills activity when app goes to background,
    // and when resume it broadcasts notification before JS is run. You can use
    // FCM.getInitialNotification() to capture those missed events.
    // initial notification will be triggered all the time even when open
    // app by icon so send some action identifier when you send notification
    FCM.getInitialNotification().then(async notif => {
      if (notif && (notif.id || notif.title || notif.type)) {
        logger.log('Started the navigation');
        store.dispatch(NotificationActions.notificationClick(true));
      }
    });

    FCM.enableDirectChannel();
    this.channelConnectionListener = FCM.on(
      FCMEvent.DirectChannelConnectionChanged,
      data => {
        logger.log('direct channel connected' + data);
      }
    );
  }

  componentWillUnmount() {
    // stop listening for events
    this.notificationListener.remove();
  }

  render() {
    return (
      <Provider store={store}>
        <InactivityTracker getNavigator={() => this.navigator}>
          <LIApp
            ref={nav => {
              this.navigator = nav;
            }}
          />
        </InactivityTracker>
      </Provider>
    );
  }
}

export default App;
