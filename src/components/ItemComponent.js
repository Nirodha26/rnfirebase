import React, { Component } from 'react';  
import { View, Text } from 'react-native';  
import PropTypes from 'prop-types';
import CommonStyles from '../styles/CommonStyles';

export default class ItemComponent extends Component {  
  static propTypes = {
    items: PropTypes.array.isRequired
  };

  render() {
    return (
      <View style={CommonStyles.itemsList}>
        {this.props.items.map((item, index) => {
          return (
            <View key={index}>
              <Text style={CommonStyles.itemtext}>{item.name}</Text>
            </View>
          );
        })}
      </View>
    );
  }
}
