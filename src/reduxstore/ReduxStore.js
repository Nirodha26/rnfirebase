/*
 * customised Redux Store functions, so that the store can be referenced from
 * non-react components (eg. Web Service class)
 */

let store;

export default {
  init(configureStore) {
    store = configureStore();
  },
  getStore() {
    return store;
  }
};
