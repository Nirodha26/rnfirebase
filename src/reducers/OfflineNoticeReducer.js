/*
 * Reducer for
 * Hiding the Offline Notice programatically
 * Shake the Offline Notice when user clicks Save buttons
 *
 */
import * as types from '../Constants/Constants.js';

export default function OfflineNoticeReducer(
  state = { shaking: false },
  action
) {
  switch (action.type) {
    case types.OFFLINE_NOTICE_SHAKE:
      return {
        ...state,
        shaking: true
      };
    default:
      return state;
  }
}
