/*
 * OfflineNotice actions for redux
 */

import * as types from '../Constants/Constants.js';

export const shakeOfflineNotice = () => ({
  type: types.OFFLINE_NOTICE_SHAKE
});
