import React, { Component } from 'react';  
import { View, Text } from 'react-native';  
import ItemComponent from '../components/ItemComponent';
import CommonStyles from '../styles/CommonStyles';

import { db } from '../config';

let itemsRef = db.ref('/items');

export default class List extends Component {  
  state = {
    items: []
  };

  componentDidMount() {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let items = Object.values(data);
      this.setState({ items });
    });
  }

  render() {
    return (
      <View style={CommonStyles.container}>
        {this.state.items.length > 0 ? (
          <ItemComponent items={this.state.items} />
        ) : (
          <Text>No items</Text>
        )}
      </View>
    );
  }
}
