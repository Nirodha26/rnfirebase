import React, { Component } from 'react';  
import {  
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  TextInput,
  Alert
} from 'react-native';

import CommonStyles from '../styles/CommonStyles';

import { db } from '../config';

let addItem = item => {  
  db.ref('/items').push({
    name: item
  });
};

export default class AddItem extends Component {  
    state = {
      name: ''
    };
  
    handleChange = e => {
      this.setState({
        name: e.nativeEvent.text
      });
    };
    handleSubmit = () => {
      addItem(this.state.name);
      Alert.alert('Item saved successfully');
    };
  
    render() {
      return (
        <View style={CommonStyles.main}>
          <Text style={CommonStyles.title}>Add Item</Text>
          <TextInput style={CommonStyles.itemInput} onChange={this.handleChange} />
          <TouchableHighlight
            style={CommonStyles.button}
            underlayColor="white"
            onPress={this.handleSubmit}
          >
            <Text style={CommonStyles.buttonText}>Add</Text>
          </TouchableHighlight>
        </View>
      );
    }
  }
  
