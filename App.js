/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';  
import { createStackNavigator, createAppContainer } from 'react-navigation';  
import Home from './src/screens/Home';

// Import Redux Store requirements
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './src/reducers';
import reduxStore from './src/ReduxStore/ReduxStore';
// we will use these two screens later in our AppNavigator
import AddItem from './src/screens/AddItem';  
import List from './src/screens/List';

const AppNavigator = createStackNavigator(  
  {
    Home,
    AddItem,
    List
  },
  {
    initialRouteName: 'Home'
  }
);

const AppContainer = createAppContainer(AppNavigator);
const configureStore = () => createStore(reducer);
reduxStore.init(configureStore);
export const store = reduxStore.getStore();

export default class App extends Component {  
  render() {
    return (
      <Provider store={store}>
       <AppContainer />
      </Provider>
    )
  }
}